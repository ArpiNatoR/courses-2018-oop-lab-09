package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix{
	
	final int nthread;
	
	public MultiThreadedSumMatrix(final int n) {
		this.nthread = n;
	}
	
	private static class Worker2 extends Thread {
		final double[][] matrix;
		private final int startposX;
		private final int startposY;
        private final int nelem;
        private long res;
        
        Worker2(final double[][] matrix, final int startX, final int startY, final int nelem) {
			super();
			this.matrix = matrix;
			this.startposX = startX;
			this.startposY = startY;
			this.nelem = nelem;
		}
        
        public void run() {
            System.out.println("Working from position [" + startposX + "][" + startposY +"] to position "
            		+ "["+ (startposX + nelem -1) + "][" + (startposY + nelem -1 +"]"));
            for (int i = startposX; i < matrix[startposX].length && i < startposX + nelem; i++) {
            	for(int j = startposY; j < matrix.length  && j < startposY + nelem; j++) {
            		this.res += this.matrix[i][j];
            	}
            }
        }
        
        public long getResult() {
            return this.res;
        }
	 }
	
	@Override
	public double sum(double[][] matrix) {
		
		final int size = matrix.length % nthread + matrix.length / nthread;
		
		final List<Worker2> workers = new ArrayList<>(nthread);
	        for (int startX = 0; startX < matrix.length; startX += size) {
	        	for(int startY = 0; startY < matrix[0].length; startY += size)
	        		workers.add(new Worker2(matrix, startX, startY, size));
	        }
	        
	        for (final Worker2 w: workers) {
	            w.start();
	        }
	        
	        long sum = 0;
	        for (final Worker2 w: workers) {
	            try {
	                w.join();
	                sum += w.getResult();
	            } catch (InterruptedException e) {
	                throw new IllegalStateException(e);
	            }
	        }
	        /*
	         * Return the sum
	         */
	        return sum;
	}

}

package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI2;

public class AnotherConcurrentGUI extends ConcurrentGUI2{
    
    static final int SLEEP_TIME=5000;
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public AnotherConcurrentGUI() {
        super();
       
        final Agent2 agent = new Agent2();
        new Thread(agent).start();
    }

    private class Agent2 implements Runnable{

        public void run() {
            try {
                Thread.sleep(SLEEP_TIME);
                getCountingAgent().stopCounting();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
    }
}

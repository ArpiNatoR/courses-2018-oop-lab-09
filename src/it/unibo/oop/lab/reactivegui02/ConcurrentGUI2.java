package it.unibo.oop.lab.reactivegui02;

import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.Dimension;

public class ConcurrentGUI2 extends JFrame{

    /**
     * 
     */
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private static final long serialVersionUID = 1L;
    
    private final JButton up = new JButton("Up");
    private final JButton down = new JButton("Down");
    private final JButton stopButton = new JButton("Stop");
    private final JLabel display = new JLabel();
    private final JPanel myPanel = new JPanel();
    private final Agent agent = new Agent();
    
    public ConcurrentGUI2() {
        
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.myPanel.setLayout(new FlowLayout());
        this.myPanel.add(display);
        this.myPanel.add(up);
        this.myPanel.add(down);
        this.myPanel.add(stopButton);
        this.getContentPane().add(myPanel);
        this.setVisible(true);
        
        new Thread(agent).start();
        
        stopButton.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Agent should be final
                agent.stopCounting();
            }
        });
        
        up.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button up.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            public void actionPerformed(ActionEvent e) {
                agent.increment();
            }
        });
        
        down.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button down.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            public void actionPerformed(ActionEvent e) {
                agent.decrement();
            }
        });
    }
    
    protected Agent getCountingAgent() {
        return agent;
    }
    
    protected class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean incrementing = true;
        private int counter;
        private volatile boolean stop;

        public void run() {
            while (!stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            ConcurrentGUI2.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    invertBehavior(); // incrementa o decrementa il contatore;
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }
        /**
         * External command to modify counter's behavior.
         */
        public void invertBehavior() {
            this.counter += incrementing ? 1 : -1;
//            if(this.incrementing) {
//                this.counter++;
//            } else {
//                this.counter--;
//            }
        }
        /**
         * External command to up button.
         */
        public void increment() {
            this.incrementing=true;
        }
        /**
         * External command to down button.
         */
        public void decrement() {
            this.incrementing=false;
        }
       
        public void stopCounting() {
            stop = true;
            SwingUtilities.invokeLater(() -> {
                up.setEnabled(true);
                down.setEnabled(true);
            });
        }
        
    }

}
